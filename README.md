# www.sozialinfo.ch #

Verwendete Tools: [Composer](https://getcomposer.org/), [Git](https://git-scm.com/ ), [Grunt](http://gruntjs.com/), [Bower](https://bower.io/)


### Development Setup ###
Setup Git Repository clonen
```
#!shell

git clone base-setup
```

Wechsle ins WebRoot
```
#!shell

cd web
```
Installiere alles mit Composer
```
#!shell

composer install
```
Wechsle ins Frontend Verzeichnis
```
#!shell

cd typo3conf/ext/base/Resources/Public/Template/
```
Installiere alle Node Module
```
#!shell
npm install
```
Installiere alle Frontend Librarys
```
#!shell

bower install
```
Go
```
#!shell

grunt
```

### Who do I talk to? ###

* mail@martinpfister.info