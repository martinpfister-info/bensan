//root namespace
var web = web || {};

//menu modul
web.menu = {

    config: {
        toggleLink: '#menu-toggle > a',
        toggleIcon: '#menu-cross-icon',
        topBar: '#top-bar',
        mainMenu: '#mainNavigation',
        address: 'address',
        footer: "footer",
        main: '#main',
        language: '#language'
    },

    binding: function() {
        var that = this;
        $(this.config.toggleLink).click(function() {
            $(that.config.toggleIcon + ',' + that.config.topBar + ',' + that.config.mainMenu).toggleClass('open');
            return false; //Prevent the browser jump to the link anchor
        });
    },

    moveNavigation: function () {
        $(this.config.mainMenu).prependTo(this.config.topBar).wrap('<div class="small-12 columns"></div>');

        $(this.config.footer).insertAfter(this.config.main);
        $(this.config.language).insertAfter(this.config.address);

        $(this.config.footer).addClass('row');
        $(this.config.address).addClass('small-8 columns');
    },

    init: function() {
        if (Foundation.MediaQuery.current == 'medium' ||
            Foundation.MediaQuery.current == 'small') {
            this.binding();
            this.moveNavigation();
        }
    }
};