/************************************************************
 *     Initializing
************************************************************/
$(function() {
    $(document).foundation();
    web.menu.init();
    web.headerImage.init();
});

// Init foundation
$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
    web.menu.init();
    web.headerImage.init();
});
