# Include original setup
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/Static/setup.txt">

# Override/extend setup
lib.fluidContent {
    templateRootPaths.10 = EXT:base/Resources/Private/Extensions/fluid_styled_content/Templates/Content/
    layoutRootPaths.10 = EXT:base/Resources/Private/Extensions/fluid_styled_content/Layouts/
    partialRootPaths.10 = EXT:base/Resources/Private/Extensions/fluid_styled_content/Partials/

    settings.media.popup {
        directImageLink = 1
        JSwindow = 0
        linkParams.ATagParams.dataWrap = data-lightbox="lb{field:uid}" data-title="{file:current:title}"
    }
}

# Cosdfasements
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:base/Configuration/TypoScript/FluidStyledElements/base_teaser.ts">

lib.fluidContent
# End Content Elements
