<?php
$tx_realurl_config = [
    'init' => [
        'enableCHashCache' => true,
        'appendMissingSlash' => 'ifNotFile,redirect',
        'adminJumpToBackend' => true,
        'enableUrlDecodeCache' => true,
        'enableUrlEncodeCache' => true,
        'emptyUrlReturnValue' => '/',
    ],
    'pagePath' => [
        'type' => 'user',
        'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
        'spaceCharacter' => '-',
        'languageGetVar' => 'L',
        'rootpage_id' => '1',
    ],
    'fileName' => [
        'defaultToHTMLsuffixOnPrev' => 1,
        'acceptHTMLsuffix' => 1,
        'index' => [
            'print' => [
                'keyValues' => [
                        'type' => 98,
                ],
            ],
        ],
     ],
    'preVars' => [
        [
            'GETvar' => 'L',
            'valueMap' => [
                'de' => '0',
                'fr' => '1',
                'it' => '2',
                'en' => '3'
            ],
            'noMatch' => 'de',
            'valueDefault' => 'de',
        ],
    ],
    'postVarSets' => [
        '_DEFAULT' => [
            'news' => [
                0 => [
                    'GETvar' => 'tx_news_pi1[news]',
                    'lookUpTable' => [
                        'table' => 'tx_news_domain_model_news',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                    ],
                ],
            ],
        ],
    ],
];

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = [
    'www.site.local' => $tx_realurl_config,
    'site.local' => $tx_realurl_config,
    'www.site.com' => $tx_realurl_config,
    'site.com' => $tx_realurl_config
];

$TYPO3_CONF_VARS['EXTCONF']['realurl']['www.site.local']['pagePath']['rootpage_id'] = 1;
$TYPO3_CONF_VARS['EXTCONF']['realurl']['site.local']['pagePath']['rootpage_id'] = 1;
$TYPO3_CONF_VARS['EXTCONF']['realurl']['www.site.com']['pagePath']['rootpage_id'] = 1;
$TYPO3_CONF_VARS['EXTCONF']['realurl']['site.com']['pagePath']['rootpage_id'] = 1;

unset($tx_realurl_config);