# Include original setup
# todo: For some reason, the indexed search ts cannot be included via FILE:EXT:indexed_search... though other sysexts work. Check up on that later.
<INCLUDE_TYPOSCRIPT: source="FILE:typo3/sysext/indexed_search/Configuration/TypoScript/setup.txt">

# Override / extend setup
<INCLUDE_TYPOSCRIPT: source="FILE:./locallangoverrides.ts">


# Plugin configuration
# Mostly copied from the original system extension.
plugin.tx_indexedsearch {

    # Set template/partial/layout paths.
    # Use original paths as fallback.
    view {
        templateRootPath >
        partialRootPath >
        layoutRootPath >

        templateRootPaths {
            10 = {$plugin.tx_indexedsearch.view.templateRootPath}
            20 = EXT:{$plugin.templatebootstrap.packageKey}/Resources/Private/Extensions/indexed_search/Templates/
        }
        partialRootPaths {
            10 = {$plugin.tx_indexedsearch.view.partialRootPath}
            20 = EXT:{$plugin.templatebootstrap.packageKey}/Resources/Private/Extensions/indexed_search/Partials/
        }
        layoutRootPaths {
            10 = {$plugin.tx_indexedsearch.view.layoutRootPath}
            20 = EXT:{$plugin.templatebootstrap.packageKey}/Resources/Private/Extensions/indexed_search/Layouts/
        }
    }

}