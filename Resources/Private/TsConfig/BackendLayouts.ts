mod {
    web_layout {
        BackendLayouts {
            SingleColumn {
                title = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.SingleColumn
                config {
                    backend_layout {
                        colCount = 1
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.mainColumn
                                        colPos = 0
                                        colspan = 1
                                        allowed = textmedia,list
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:base/Resources/Public/Backend/Icons/Layouts/SingleColumn.png
            }
            SingleColumnTeaser {
                title = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.SingleColumnTeaser
                config {
                    backend_layout {
                        colCount = 1
                        rowCount = 3
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.mainColumn
                                        colPos = 0
                                        allowed = textmedia,list
                                    }
                                }
                            }
                            2 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.teaserColumn
                                        colPos = 20
                                        allowed = base_teaser
                                    }


                                }
                            }
                            3 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.mainColumn
                                        colPos = 5
                                        allowed = textmedia,list
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:base/Resources/Public/Backend/Icons/Layouts/SingleColumnTeaser.png
            }
            TwoColumns {
                title = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.TwoColumns
                config {
                    backend_layout {
                        colCount = 2
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.mainColumn
                                        colPos = 0
                                        colspan = 1
                                        allowed = textmedia,list
                                    }
                                    2 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.rightColumn
                                        colPos = 1
                                        colspan = 0
                                        allowed = textmedia,list
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:base/Resources/Public/Backend/Icons/Layouts/TwoColumns.png
            }
            SysFolder {
                title = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.SySFolder
                config {
                    backend_layout {
                        colCount = 0
                        rowCount = 0
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:base/Resources/Private/Language/Backend.xlf:backend_layout.SysFolder
                                        colPos = 1
                                        colspan = 1
                                        allowed =
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:base/Resources/Public/Backend/Icons/Layouts/SysFolders.png
            }
        }
    }
}