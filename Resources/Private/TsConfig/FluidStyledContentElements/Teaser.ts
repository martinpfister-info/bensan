
# **************************************************
# Add the slider to the "New Content Element Wizard"
# **************************************************
mod.wizards.newContentElement.wizardItems.common {
    elements {
        base_teaser {
            iconIdentifier = content-image
            title = Teaser
            description = Teaser Element

            tt_content_defValues {
                CType = base_teaser
            }
        }
    }
    show := addToList(base_teaser)
}