# **********************************************************
#   This setup generates all the needed icon images
#   for the site (excluding favicon which needs to
#   be generated locally (grunt).
#
#   Structure (roughly):
#   - 10 Loads REGISTER with icon source file path
#   - 20 The main icon COA. Contains all (!) icons in
#        different sizes based on $site.icons constants.
#   - 20.[10-100]   All "regular" png icons.
#   - 20.200        .ico file containing multiple sizes
#   - 20.300        Microsoft tile image & color.
# **********************************************************
page.headerData.20 = COA
page.headerData.20 {

    # **********************************************************
    #  Favicon
    #  Will not be dynamically generated on the webserver as
    #  T3 might use GraphicsMagick, which does not support ICO!
    # **********************************************************
    50 = TEXT
    50 {
        prepend = TEXT
        prepend.char = 10
        stdWrap.data = PATH:EXT:base/Resources/Public/Template/images/favicon.ico
        wrap = <link href="|" rel="shortcut icon" type="image/x-icon">
        required = 1
        stdWrap.required = 1
    }
}