page {
    # CSS files to be included
    includeCSS {
        mainCSS = EXT:base/Resources/Public/Template/css/app.css
        mainCSS.media = screen,print
    }
    includeJSlibs {
        jQuery = EXT:base/Resources/Public/Template/js/frameworks/jquery/jquery.min.js
        #foundation             = EXT:base/Resources/Public/Template/js/frameworks/foundation/foundation.core.js
    }
    # JS libs footer
    includeJSFooterlibs {
        foundation             = EXT:base/Resources/Public/Template/js/frameworks/foundation/foundation.min.js
        #foundationMediaQuerys  = EXT:base/Resources/Public/Template/js/frameworks/foundation/foundation.util.mediaQuery.js
        #foundationTabs         = EXT:base/Resources/Public/Template/js/frameworks/foundation/foundation.tabs.js
        lightbox               = EXT:base/Resources/Public/Template/js/frameworks/lightbox/lightbox.js
    }

    # JS footer
    includeJSFooter {
        # Plugins
        menuJS        = EXT:base/Resources/Public/Template/js/menu.js
        headerImageJS = EXT:base/Resources/Public/Template/js/headerImage.js
        # Main application
        mainJS        = EXT:base/Resources/Public/Template/js/app.js
    }

}

# **********************************************************
# Google analytics (conditionally)
# **********************************************************
[globalVar = LIT:1 = {$site.googleAnalytics}]
    page.footerData.100 = COA
    page.footerData.100 {
        stdWrap.wrap = <script>|</script>

        10 = TEXT
        10.value (
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})
            (window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', '{$site.googleAnalytics.account}', 'auto');
            ga('send', 'pageview');
        )
    }
[global]

# Remove analytics, if user has set the 'Do Not Track' flag in the browser
[Pfister\Base\Utility\Condition\DoNotTrackCondition]
    page.footerData.100 >
[global]
