#-------------------------------------------------------------------------------
#    Logo
#-------------------------------------------------------------------------------
lib.logo = COA
lib.logo {

    # Main logo
    10 = IMAGE
    10 {
        file = EXT:base/Resources/Public/Template/images/assicurama-definitiv.jpg
        file {
            maxW = 600
            ext = png
            # Add pseudo crop parameter to force re-rendering as png
            crop = 0.1,0.1
        }

        altText = assicurama logo
        altText.insertData = 1
        stdWrap.typolink {
            title.data = LLL:EXT:base/Resources/Private/Language/locallang.xlf:home
            parameter = 1
            ATagParams = id="logo" class="show-for-medium-up"
        }
    }


    # Alternative logo on narrow/small screens
    #20 < .10
    #20.file = EXT:{$plugin.templatebootstrap.packageKey}/Resources/Private/LogoSources/icon-default.ai
    #20.file.maxW >
    #20.file.maxH = 100
    #20.stdWrap.typolink.ATagParams = id="logo-iconesque"

}