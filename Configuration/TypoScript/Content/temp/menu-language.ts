temp.menu-language = HMENU
temp.menu-language {
    special = language
    special.value = {$site.languageUids}
    special.normalWhenNoLanguage = 0

    addQueryString = 1
    addQueryString.exclude = L,cHash

    wrap = <ul>|</ul>

    1 = TMENU
    1 {
        noBlur = 1

        # Normal link to language that exists
        NO = 1
        NO {
            wrapItemAndSub = <li>|</li>
            stdWrap.cObject = TEXT
            stdWrap.cObject.value = {$site.languageLabels}
        }

        # Active
        ACT = 1
        ACT < .NO
        ACT {
            ATagParams = class="active"
            wrapItemAndSub = <li class="active">|</li>
        }

        # Language that is NOT available:
        USERDEF1 < .NO
        USERDEF1.doNotLinkIt = 1
    }
}
