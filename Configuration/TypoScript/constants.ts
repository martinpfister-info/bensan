# **********************************************************
#    Extension constants
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:./../../Resources/Private/Extensions/fluid_styled_content/constants.ts">

# **********************************************************
#    Company constants
# **********************************************************
company {
    name = Bensan AG
    street = Glütschbachstrasse 2
    pobox =
    city =  3661 Uetendorf
    phone = +41 31 318 01 60
    email = ibrahim.ben@bensan.ch
}

# **********************************************************
#    Site settings
# **********************************************************
site {

    url = http://www.bensan.ch

    enableRealURL             = 1
    enableSocialMediaMetaTags = 0

    # Page title
    pageTitlePrefix < company.name
    pageTitlePrefix := appendString( -)
    pageTitleSuffix =

    # Languages
    languageUids = 0,1
    languageLabels = DE |*| FR

    # Compress / merge CSS / JS
    compressAndMergeAssets = 1

    # Google analytics
    googleAnalytics = 0
    googleAnalytics.account = UA-XXXXXX

}


# **********************************************************
#    Image rendering settings
# **********************************************************
# Set this the the max width ANY rendered image should have. Will
# be downsized by browser when needed. Usually, this is the
# main content column width.
imageRenderingMaxWidth = 1200
# This is the same figure, but when the images appear side-by-side
# with the text. This should correspond with the max-width of the
# gallery column set in _common.scss (usually 70%).
imageRenderingMaxWidthInText = 840