<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

# Adding page and user tsconfig
$pageTSConfig = "";
foreach (['BackendLayouts.ts', 'Page.ts', 'RTE.ts'] as $fileName) {
    $pageTSConfig .= GeneralUtility::getUrl(
        GeneralUtility::getFileAbsFileName('EXT:' . $_EXTKEY . '/Resources/Private/TsConfig/' . $fileName)
    );
}
ExtensionManagementUtility::addPageTSConfig($pageTSConfig);

# Adding GridElements tsconfig
foreach (['BlockGrids.ts'] as $fileName) {
    $pageTSConfig .= GeneralUtility::getUrl(
        GeneralUtility::getFileAbsFileName('EXT:' . $_EXTKEY . '/Resources/Private/TsConfig/Gridelements/' . $fileName)
    );
}
ExtensionManagementUtility::addPageTSConfig($pageTSConfig);

# Adding FluidStyledContent tsconfig
foreach (['Teaser.ts'] as $fileName) {
    $pageTSConfig .= GeneralUtility::getUrl(
        GeneralUtility::getFileAbsFileName('EXT:' . $_EXTKEY . '/Resources/Private/TsConfig/FluidStyledContentElements/' . $fileName)
    );
}
ExtensionManagementUtility::addPageTSConfig($pageTSConfig);

$userTSConfigAbsFileName = GeneralUtility::getFileAbsFileName(
    'EXT:' . $_EXTKEY . '/Resources/Private/TsConfig/User.ts'
);
ExtensionManagementUtility::addUserTSConfig(GeneralUtility::getUrl($userTSConfigAbsFileName));

ExtensionManagementUtility::addTypoScriptSetup(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'. $_EXTKEY .'/Configuration/TypoScript/setup.ts">'
);
ExtensionManagementUtility::addTypoScriptConstants(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'. $_EXTKEY .'/Configuration/TypoScript/constants.ts">'
);