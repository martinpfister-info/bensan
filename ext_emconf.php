<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Bensan Base Extension',
    'description' => '',
    'category' => 'fe',
    'shy' => 0,
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => 0,
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author' => 'Martin Pfisteer',
    'author_email' => 'template@martinpfister.info',
    'author_company' => '',
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'version' => '0.0.1',
    '_md5_values_when_last_written' => '',
    'constraints' => [
        'depends' => [
            'php' => '5.4.0-0.0.0',
            'typo3' => '7.6.0-7.99.99',
            'fluid' => '7.6.0-7.99.99',
        ],
        'conflicts' => [],
        'suggests' => [
            'realurl' => '1.12.1-',
            'gridelements' => '3.0.0-',
        ],
    ],
    'autoload' =>
        [
            'psr-4' => [
                'MartinPfister\\Base\\' => 'Classes',
            ],
            'classmap' => [
                'Classes'
            ]
        ]
];
